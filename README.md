# A03 Assignment - Website

My personal website for Developing Web Applications and Services. Northwest Missouri State University. 

## How to use

Open a command window in your project folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)
```
> npm install
> node gbapp.js
```
Point your browser to `http://localhost:8081`.