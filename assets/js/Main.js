var App = {
    addNumbers: function (firstNum, secondNum){
        if (firstNum == "" || secondNum == ""){ throw Error(/The given argument is not a number/)};
        if (isNaN(firstNum) || isNaN(secondNum)){ throw Error(/The given argument is not a number/)};
        if(firstNum == null || secondNum == null){throw Error(/The given argument is not a number/)}
        if(isNaN(firstNum) || isNaN(secondNum)){throw Error(/The given argument is not a number/)}
        

        firstNum = parseInt(firstNum);
        secondNum = parseInt(secondNum);

        result = firstNum + secondNum;

        return result;
    }
}
$(document).ready(function(){
    $("#submit").click(function(){
    //Variables
    var firstNum = $("#firstInput").val();
    var secondNum = $("#secondInput").val();
    
    //Main section of code
    var sum = addNumbers(firstNum, secondNum);

    alert("The sum of " + firstNum + " and " + secondNum + " is: " + sum);
    });
    
    //Functions
    function addNumbers(firstNum, secondNum){
        if (firstNum == "" || secondNum == ""){ alert("Please enter valid numbers!")};
        if (isNaN(firstNum) || isNaN(secondNum)){ alert("Please enter valid numbers!")};

        firstNum = parseInt(firstNum);
        secondNum = parseInt(secondNum);

        result = firstNum + secondNum;

        return result;
    }


});